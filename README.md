# Onecss

A lightweight CSS framework for and by Onepoint.  
Takes bits of KnaCSS framework like grillade.css, print, base font and reset styles.  
See Raphaël Goetter's [KnaCSS](https://github.com/alsacreations/KNACSS)

## Overview

Look into [sample.html](https://onepoint.gitlab.io/onecss/sample.html) to obtain simple code samples by element.  
Look into [showcase.html](https://onepoint.gitlab.io/onecss/showcase.html) to simply visualize elements.

### Material Icons

You can search for available icons from [Material Icons](https://material.io/icons/).

## Settings

Everything customizable and global is in `/settings/variables.css`

## Generic

OneCSS uses `normalize.css` and a custom, small `reset.css` to harmonize components across browsers.
Printed version is supported thanks to a `print.css`.

## Elements

Some elements have a global style as their appearance is not subject to variations or declinations.  
**Yes, those elements are semantic enough to be related to a specific look.**  
Overriding is still terribly easy as global selectors *don't weight anything.*

* `hr` (elements/content.css)
* `mark` (elements/typography.css)
* `h1`, `h2`, `h3`, `h4`, `h5`, `h6` (elements/typography.css)
* `a` (elements/typography.css)
* `table`, `thead`, `th`, `td`, `tfoot`, `caption` (elements/table.css)
* `fieldset`, `legend` (elements/form.css)
* `button`, `select`, `input`, `textarea` (elements/form.css)

### Pseudo-Elements

* Placeholders are stylized in `elements/placeholder.css`
* Selection of text is stylized in `elements/rules.css`
* Webkit scrollbars are stylized in `generic/scrollbar.css`

## Layout

### Grid framework

`grillade.css` is a grid micro-framework by Raphaël Goetter, see [documentation](https://knacss.com/grillade/)

### Basic container

`.container` is made to contain floats or absolute positionned elements; it comes with a padding and automatic vertical spacing between elements.

### Handling layers

`layers.css` allows you to avoid bad z-index positioning and centralizes every z-index declaration in one file.

### Adding your code

`layout.css` can be extended if you're likely to add reusable stuff like navbars, footers and such.  
For ultra-specific code, you may add a `08_pages` folder and write in new files what's specific to your pages.

## Components

### Animation

Every animation used in the framework is cooked into `utilities/animation.css`

### Buttons

Aside from generic buttons, you can made stylized buttons out of links or anything else, with the classes :

* `.button`
* `.button-primary`
* `.button-neutral`
* `.button-secondary`
* `.button-heavy`
* `.button-heavy`
* `.button-accent`
* `.button-mild`

You can also modulate their sizes by adding those classes onto them :

* `.size-l`
* `.size-m`
* `.size-s`
* `.size-xs`
* `.size-xxs`

Feel free to add an icon into any button, like this :

`<button class="button-accent"><i class="icon-user"></i> Button label</button>`

### Decorated inputs

Aside of generic inputs that are already globally styled, you may want to include an icon into an input. Use `.input-decorated`.

`<label for="input" class="input-decorated"><span class="icon">iconname</span><input name="input"></label>`

### Messages

Messages are full width blocks that are contained in the flow of the page. They indicate a durable or temporary information.

`<div class="message">My message.</div>`

They're declined in colors just as buttons are, and appear with opening animation. To animate their closing, add the class `.out` on it. To prevent any animation, add the class `._inert`.

* `.message-primary`
* `.message-neutral`
* `.message-secondary`
* `.message-heavy`
* `.message-heavy`
* `.message-accent`
* `.message-mild`

### Switch

Switch are, technically, checkboxes, but their appearance is one of a switch button.

Implemented a switch with the following code :

`<label for="input" class="switch"><input type="checkbox" name="input" /><span></span></label>`

And decline in any desired color :

* `.switch-primary`
* `.switch-neutral`
* `.switch-secondary`
* `.switch-heavy`
* `.switch-heavy`
* `.switch-accent`
* `.switch-mild`

### Toasts

Toasts resemble messages in their usage, but are stuck to the bottom of their container, out of the flow and hover the content.

`<div class="toast">This is my toast.</div>`

Remove any animation just as for messages, with `._inert`. Vary color by using the colored classes :

* `.toast-primary`
* `.toast-neutral`
* `.toast-secondary`
* `.toast-heavy`
* `.toast-heavy`
* `.toast-accent`
* `.toast-mild`

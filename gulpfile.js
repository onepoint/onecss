const path = require('path');
const gulp = require('gulp-help')(require('gulp'));
const myth = require('gulp-myth');
const insert = require('gulp-insert');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const minify = require('gulp-clean-css');
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');

/* Variables */
const $dest = './dist/';
const $src = './src/';
const $basepath = './src/style/';
const $filename = 'one.css';

const $variables = '../01_settings/01_variables.css';
const $styles = [
        $basepath + '00_vendor/**/*.css',
        $basepath + '01_settings/**/*.css',
        $basepath + '02_tools/**/*.css',
        $basepath + '03_generic/**/*.css',
        $basepath + '04_elements/**/*.css',
        $basepath + '05_objects/**/*.css',
        $basepath + '06_components/**/*.css',
        $basepath + '07_utilities/**/*.css'
    ];

/* Default */
gulp.task('default', ['vendor', 'assets', 'build', 'prod', 'watch']);
gulp.task('watch', function () {
    gulp.watch($styles, ['prod']);
});

/* Dependencies */
gulp.task('vendor', 'Copies dependencies to appropriate folder', function () {
    const vendor = [
        "./node_modules/normalize.css/normalize.css"
    ];
    return gulp.src(vendor).pipe(gulp.dest('./src/style/00_vendor/')); // @FIXME
});

/* Assets */
gulp.task('assets', 'Copies assets to dist folder', function () {
    return gulp.src($src + 'assets/**/*')
                .pipe(gulp.dest($dest + 'assets/'));
});

/* Preprocessing */
gulp.task('build', 'Compiles any file that needs pre-processing', function () {
    return gulp.src($src + '**/*.scss', {base: "./"})
            .pipe(sass())
            .pipe(gulp.dest("."))
});


/* Development */
gulp.task('dev', 'Builds source mapped version', function () {
    return gulp.src($styles)
        .pipe(sourcemaps.init())
        .pipe(insert.append('@import url("' + $variables + '");'))
        .pipe(myth())
        .pipe(concat($filename))
        .pipe(rename({
            suffix: ".map"
        }))
        .pipe(gulp.dest($dest));
});

/* Staging */
gulp.task('staging', 'Builds prefixed, unminified version', function () {
    return gulp.src($styles)
        .pipe(insert.append('@import url("' + $variables + '");'))
        .pipe(myth())
        .pipe(concat($filename))
        .pipe(gulp.dest($dest));
});

/* Production */
gulp.task('prod', 'Builds prefixed, minified version', function () {
    return gulp.src($styles)
        .pipe(concat($filename))
        .pipe(myth())
        .pipe(minify())
        .pipe(rename({
            suffix: ".min"
        }))
        .pipe(gulp.dest($dest));
});
